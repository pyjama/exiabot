const discord=require('discord.js')
const client=new discord.Client()

const conf=require('./config.json')
const guilds=require('./data/guilds.json')
const ignored=require('./data/ignored.json')
const autorep=require('./data/autorep.json')
const kick=require('./data/kick.json')
const cmd=require('./src/cmd.js')
const misc=require('./src/misc.js')

let startTimestamp=0


client.on('ready', ()=>{
    startTimestamp=new Date().getTime()/1000|0
    client.user.setActivity(conf.playing)
    console.log(conf.playing)
})

client.on('message', msg=>{
    if(msg.channel.type=='text' && msg.author.bot==false){

        let guildid='a'+msg.guild.id  // 'a' because variable names can't start with a number

        if(!guilds[guildid]||!autorep[guildid]){ // If the bot doesn't know the server the message is from
            misc.addGuild(guildid, guilds, autorep, kick)
        }

        if(!ignored.includes(msg.channel.id)){

            if(msg.content===conf.helpcmd)msg.channel.send(misc.help(guilds, guildid, cmd))

            if(msg.content.startsWith(guilds[guildid].prefix)){  // command parser
                let command=msg.content.substr(guilds[guildid].prefix.length)
                command=msg.content.match(/[A-zÀ-ÿ]+/)[0]
                let arg=msg.content.substr(msg.content.match(/[A-zÀ-ÿ]+/)[0].length+2)
                msg.isCmd=true
                switch(command){
                    case 'coconérateur':
                        msg.channel.send(cmd.coconerateur())
                        break
                    //case 'setprefix':
                        //msg.channel.send(cmd.setprefix(arg, guilds, guildid))
                        //break
                    case 'setlanguage':
                        msg.channel.send(cmd.setlanguage(arg, guilds, guildid))
                        break
                    case 'stfu':
                        msg.channel.send(cmd.stfu(ignored, msg.channel.id, guilds, guildid))
                        break
                    case 'uptime':
                        msg.channel.send(cmd.uptime(startTimestamp))
                        break
                    case 'addresponse':
                        msg.channel.send(cmd.addresponse(arg, autorep, guilds, guildid, msg.guild, misc.removeMentions))
                        break
                    case 'delresponse':
                        msg.channel.send(cmd.delresponse(arg, autorep, guilds, guildid))
                        break
                    case 'delall':
                        msg.channel.send(cmd.delall(msg.member, autorep, guilds, guildid))
                        break
                    case 'votekick':
                        msg.channel.send(cmd.votekick(msg,guilds, guildid, conf.votekick.time, conf.votekick.count, misc.getUserRoles))
                        break
                    default:
                        msg.isCmd=false
                }
            }
            misc.autorep(msg, guildid, autorep)
        }else{
            if(msg.content===guilds[guildid].prefix+'talk'){
                msg.channel.send(cmd.stfu(ignored, msg.channel.id, guilds, guildid))
            }
        }
    }
})

client.on('messageDelete', msg=>{
    if(msg.channel.type==='text' && msg.author.bot===false){
        if(msg.isCmd && !ignored.includes(msg.channel.id)){
            msg.content=misc.removeMentions(msg.guild, msg.content)
            msg.channel.send('**"'+msg.content+'"**\n    *<@'+msg.author.id+'>, '+misc.getDatetimeStr(msg.createdAt)+'*')
        }
    }
})

client.on('messageUpdate', (oldmsg, msg)=>{
    if(msg.channel.type==='text' && msg.author.bot===false){
        if(msg.isCmd && !ignored.includes(msg.channel.id)){
            msg.content=oldmsg.content
            msg.channel.send('**"'+msg.content+'"**\n    *<@'+msg.author.id+'>, '+misc.getDatetimeStr(msg.createdAt)+'*')
        }
    }
})


client.on('guildMemberAdd', member=>{
    setTimeout(()=>{
        if(kick.kicked['a'+member.guild.id][member.id]){
            kick.kicked['a'+member.guild.id][member.id].forEach(role=>member.addRole(role))
            delete kick.kicked['a'+member.guild.id][member.id]
        }
    }, conf.votekick.roleTimeout*1000)
})

client.on('messageReactionAdd', (reac, user)=>{
    if(conf.selfReactionKick.includes(reac.emoji.id) && user.id === reac.message.author.id){
        reac.message.member.kick()
    }
})

client.on('error', err=>{
    client.login(conf.token)
    if(conf.errorChannel) client.channels.get(conf.errorChannel).send(err.message)
})


client.login(conf.token)
